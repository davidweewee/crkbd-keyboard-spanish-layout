# davidweewee

Disposición en español de teclado CRKBD, basado en [Gotham de qmk firmware](https://github.com/qmk/qmk_firmware/tree/master/keyboards/crkbd/keymaps/gotham)  
Diseñado para trabajo en oficina.

## Capas

1. **QWERTY**: Capa base. Teclado Qwerty normal salvo por la ausencia de teclas que se suplen con pulsaciones cortas y largas en algunas de ellas:
    - Una pulsación breve en mayúsculas activa el bloqueo y una pulsación larga actúa normal.
    - Una pulsación corta en la tecla de la tilde actúa normal y una pulsación larga mayúsculas.
    - Tecla de suprimir en la última fila al final.
    - Espacio en la segunda tecla especial y pulsación larga la tecla súper.
    - Enter en la tercera tecla especial y Alt derecho en pulsación larga.
    - Capa **CUR_MACROS** en la cuarta tecla especial.
    - Capa de **NUM_ESPECIAL** en la última tecla especial.

2. **CUR_MACROS**: Cursores en mano derecha más teclas de posicionamiento (inicio, fin, avance página...) y macros en la izquierda, prácticos para escribir textos recurrentes en oficina (en mi caso uso abreviaturas "-SIGNOGV-" que se convierten en textos largos usando autocompletadores en procesadores de texto).

3. **NUM_ESPECIAL**: Teclado numérico en mano derecha y teclas especiales que no entraban en QWERTY:
    - Tecla ordinal.
    - Tecla <.
    - Cedilla en la C.
    - Comillas simples.
    - Apertura de admiración.
    - Alt derecho.

## OLED
Esta disposición incluye una fuente para OLED. Se puede activar o desactivar en el archivo rukes.mk.

## Flashing
Para introducir el código en Pro Micro utiliza `make crkbd:davidweewee:avrdude`, y en Elite-C `make crkbd:davidweewee:dfu`.

# Disposición de teclas por capas
## QWERTY
    ┌───┬───┬───┬───┬───┬───┐      ┌───┬───┬───┬───┬───┬───┐  
    │TAB│ Q │ W │ E │ R │ T │      │ Y │ U │ I │ O │ P │BOR│  
    ├───┼───┼───┼───┼───┼───┤      ├───┼───┼───┼───┼───┼───┤  
    │E/A│ A │ S │ D │ F │ G │      │ H │ J │ K │ L │ Ñ │ENT│  
    ├───┼───┼───┼───┼───┼───┤      ├───┼───┼───┼───┼───┼───┤  
    │SBK│ Z │ X │ C │ V │ B │      │ N │ M │ , │ . │ - │ ' │  
    └───┴───┴───┴─┬─┴─┬─┴─┬─┴─┐  ┌─┴─┬─┴─┬─┴─┬─┴───┴───┴───┘  
                  │CTL│S/G│CUR│  │NUM│SPC│RAL│  
                  └───┴───┴───┘  └───┴───┴───┘  
## CUR_MACROS
    ┌───┬───┬───┬───┬───┬───┐      ┌───┬───┬───┬───┬───┬───┐  
    │M1 │M2 │M3 │M4 │M5 │   │      │/HM│   │/UP│   │/PU│BOR│  
    ├───┼───┼───┼───┼───┼───┤      ├───┼───┼───┼───┼───┼───┤  
    │M6 │M7 │M8 │M9 │M10│   │      │   │/LT│/DN│/RT│   │SUP│  
    ├───┼───┼───┼───┼───┼───┤      ├───┼───┼───┼───┼───┼───┤  
    │SFT│M11│M12│M13│   │   │      │/ED│   │   │   │/PD│SFT│  
    └───┴───┴───┴─┬─┴─┬─┴─┬─┴─┐  ┌─┴─┬─┴─┬─┴─┬─┴───┴───┴───┘  
                  │CTL│S/G│XXX│  │   │SPC│RAL│  
                  └───┴───┴───┘  └───┴───┴───┘  

## NUM_ESPECIAL
    ┌───┬───┬───┬───┬───┬───┐        ┌───┬───┬───┬───┬───┬───┐  
    │ º │ ' │ { │ } │ [ │ ] │        │ * │ 7 │ 8 │ 9 │ - │BOR│  
    ├───┼───┼───┼───┼───┼───┤        ├───┼───┼───┼───┼───┼───┤  
    │CAL│   │   │   │ ¡ │ ! │        │ / │ 4 │ 5 │ 6 │ + │ENT│  
    ├───┼───┼───┼───┼───┼───┤        ├───┼───┼───┼───┼───┼───┤  
    │SFT│ < │   │ Ç │ ¿ │ ? │        │ 0 │ 1 │ 2 │ 3 │ , │ . │  
    └───┴───┴───┴─┬─┴─┬─┴─┬─┴─┐    ┌─┴─┬─┴─┬─┴─┬─┴───┴───┴───┘  
                  │CTL│S/G│   │    │XXX│SPC│RAL│  
                  └───┴───┴───┘    └───┴───┴───┘  

# Leyenda

ESC = Escape  
BOR = Borrar  
SUP = Suprimir  
TAB = Tabulador  
SFT = Mayúsculas  
SPC = Espacio
E/A = Una pulsación escape, pulsación mantenida Alt izquierdo   
SBK = Una pulsación Bloqueo mayúsculas, mantenida mayúsculas.
CTL = Control  
S/G = Una pulsación espacio, pulsación mantenida super  
CAL = Calculadora
CUR = capa CUR_MACROS  
NUM = Capa NUM_ESPACIAL  
Mx  = Macro número x  
/HM = Inicio  
/ED = Fin  
/UP = Cursor arriba  
/DN = Cursor abajo
/LT = Cursor izquierda  
/RT = Cursor derecha  
/PU = Página arriba  
/PD = Página abajo  
RAL = Alt derecho  
XXX = Tecla pulsada