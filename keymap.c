#include QMK_KEYBOARD_H
#include "keycodes.h"
#include "keymap_spanish.h"
#include "sendstring_spanish.h"

#ifdef OLED_ENABLE
#    include "oled.c"
#endif

#if defined(RGBLIGHT_ENABLE) || defined(RGB_MATRIX_ENABLE)
#    include "rgb.c"
#endif

// clang-format off
const uint16_t PROGMEM keymaps[][MATRIX_ROWS][MATRIX_COLS] = {
  [_QWERTY] = LAYOUT_split_3x6_3(
  //|-----------------------------------------------------|                    |-----------------------------------------------------|
     KC_TAB,      ES_Q,    ES_W,    ES_E,    ES_R,    ES_T,                         ES_Y,    ES_U,    ES_I,    ES_O,    ES_P,  KC_BSPC,
  //|--------+--------+--------+--------+--------+--------|                    |--------+--------+--------+--------+--------+--------|
     ESC_LALT,    ES_A,    ES_S,    ES_D,    ES_F,    ES_G,                         ES_H,    ES_J,    ES_K,    ES_L,  ES_NTIL, KC_ENT,
  //---------+--------+--------+--------+--------+--------|                    |--------+--------+--------+--------+--------+--------|
     LSFT_CAPS,   ES_Z,    ES_X,    ES_C,    ES_V,    ES_B,                         ES_N,    ES_M,    ES_COMM, ES_DOT,ES_MINS, ES_ACUT,
  //---------+--------+--------+--------+--------+--------+--------|  |--------+--------+--------+--------+--------+--------+--------|
                                        KC_LCTL, SPC_LGUI,CUR_MACROS, NUM_ESPECIAL, KC_SPC, KC_RALT
                                      //|--------------------------|  |--------------------------|


  ),

  [_CUR_MACROS] = LAYOUT_split_3x6_3(
  //|-----------------------------------------------------|                    |-----------------------------------------------------|
     SIGNADOGV, FIRMASGV, FIRMOGV, FIRMAGV, RUBRICAGV, XXXXXXX,                     KC_HOME, XXXXXXX, KC_UP,   XXXXXXX,  KC_PAGE_UP, _______,
  //|--------+--------+--------+--------+--------+--------|                    |--------+--------+--------+--------+--------+--------|
     SIGNADOFL, FIRMASFL, FIRMOFL, FIRMAFL, RUBRICAFL, XXXXXXX,                     XXXXXXX, KC_LEFT, KC_DOWN, KC_RIGHT, XXXXXXX, KC_DEL,
  //|--------+--------+--------+--------+--------+--------|                    |--------+--------+--------+--------+--------+--------|
     KC_LSFT,  BANCO1, BANCO2, BANCO3, XXXXXXX, XXXXXXX,                       KC_END,  XXXXXXX, XXXXXXX, XXXXXXX,  KC_PAGE_DOWN, KC_RSFT,
  //|--------+--------+--------+--------+--------+--------+--------|  |--------+--------+--------+--------+--------+--------+--------|
                                          _______, _______, _______,   _______, _______, _______
                                      //|--------------------------|  |--------------------------|
    ),

  [_NUM_ESPECIAL] = LAYOUT_split_3x6_3(
  //|-----------------------------------------------------|                    |-----------------------------------------------------|
      ES_MORD, ES_QUOT, ES_LCBR, ES_RCBR, ES_LBRC, ES_RBRC,                      KC_PAST,   KC_7,    KC_8,   KC_9,  KC_PMNS,  ES_EQL,
  //|--------+--------+--------+--------+--------+--------|                    |--------+--------+--------+--------+--------+--------|
      KC_CALC, XXXXXXX, XXXXXXX, XXXXXXX, ES_IEXL, ES_EXLM,                      KC_PSLS,   KC_4,    KC_5,   KC_6,  KC_PPLS,  _______,
  //|--------+--------+--------+--------+--------+--------|                    |--------+--------+--------+--------+--------+--------|
      _______, ES_LABK, XXXXXXX, ES_CCED, ES_IQUE, ES_QUES,                      KC_0,      KC_1,    KC_2,   KC_3,  ES_COMM ,  ES_DOT,
  //|--------+--------+--------+--------+--------+--------+--------|  |--------+--------+--------+--------+--------+--------+--------|
                                          _______, _______, _______,   _______, _______, _______
                                      //|--------------------------|  |--------------------------|
  )

};
// clang-format off

#if defined(RGBLIGHT_ENABLE) || defined(RGB_MATRIX_ENABLE)
layer_state_t layer_state_set_user(layer_state_t state) {
    /* For any layer other than default, save current RGB state and switch to layer-based RGB */
    if (layer_state_cmp(state, 0)) {
        restore_rgb_config();
    } else {
        uint8_t layer = get_highest_layer(state);
        if (layer_state_cmp(layer_state, 0)) save_rgb_config();
        rgb_by_layer(layer);
    }
    return state;
}
#endif

bool process_record_user(uint16_t keycode, keyrecord_t *record) {
#ifdef OLED_ENABLE
    if (record->event.pressed) {
        oled_timer = timer_read();
        is_key_processed = true;
        add_keylog(keycode);
    }
#endif

    switch (keycode) {
        case CUR_MACROS:
            if (record->event.pressed) {
                layer_on(_CUR_MACROS);
            } else {
                layer_off(_CUR_MACROS);
            }
            return false;
        case NUM_ESPECIAL:
            if (record->event.pressed) {
                layer_on(_NUM_ESPECIAL);
            } else {
                layer_off(_NUM_ESPECIAL);
            }
            return false;
    }

    // MACROS
    if (record->event.pressed) {
        switch(keycode) {
            case SIGNADOGV:
                SEND_STRING("-SIGNADOGV-");    
                return false;
            case SIGNADOFL:
                SEND_STRING("-SIGNADOFL-");
                return false;
            case FIRMASGV:
                SEND_STRING("-FIRMASGV-");
                return false;
            case FIRMOGV:
                SEND_STRING("-FIRMOGV-");
                return false;
            case FIRMAGV:
                SEND_STRING("-FIRMAGV-");
                return false;
            case RUBRICAGV:
                SEND_STRING("-RUBRICAGV-");
                return false;   
            case FIRMASFL:
                SEND_STRING("-FIRMASFL-");
                return false;
            case FIRMOFL:
                SEND_STRING("-FIRMOFL-");
                return false;
            case FIRMAFL:
                SEND_STRING("-FIRMAFL-");
                return false;
            case RUBRICAFL:
                SEND_STRING("-RUBRICAFL-");
                return false;           
            case BANCO1:
                SEND_STRING("-BANCO1-");
                return false;
            case BANCO2:
                SEND_STRING("-BANCO2-");
                return false;
            case BANCO3:
                SEND_STRING("-BANCO3-");
                return false;
            }
    }

    return true;
}
